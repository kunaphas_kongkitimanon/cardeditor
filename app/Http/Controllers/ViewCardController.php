<?php
/**
 * Created by IntelliJ IDEA.
 * User: ICT21
 * Date: 12/23/2016
 * Time: 1:04 PM
 */

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Model\deck_name;
use App\Http\Model\Card;
class ViewCardController extends Controller
{

    public function __construct()
    {

    }

    public function index(){
        //get decks
        $decks = deck_name::all();

        return view('pages.view_card')
            ->with('decks', $decks);

    }

    public function getDecks(){
        $decks = deck_name::select('deck_id','deck_name', 'deck_description', 'updated_date')->get();
        return response()->json($decks);
    }

    public function dropDeck(Request $request){
        try{
            if ($request->isMethod('post')){
                $deck_name = $request->input( 'deck_name' );

                DB::beginTransaction();
                $cards = Card::where('deck_name', $deck_name)
                    ->delete();
                $decks = deck_name::where('deck_name', $deck_name)
                    ->delete();
                DB::commit();
                $response = array(
                    'status' => 'success',
                    'msg' => 'Deck deleted successfully',
                );
            }else{
                $response = array(
                    'status' => 'BadRequest',
                    'msg' => 'Request not allowed',
                );
            }

            return response()->json($response);

        }catch (\Exception $e){
            $response = array(
                'status' => 'error',
                'msg' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
            );
            return response()->json($response);

        }

    }

}