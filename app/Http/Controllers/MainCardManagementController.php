<?php
/**
 * Created by PhpStorm.
 * User: Note
 * Date: 6/3/2017
 * Time: 10:48 AM
 */

namespace App\Http\Controllers;
use App\Http\Model\card_frame;
use App\Http\Model\font;
use Illuminate\Http\Request;
use App\Http\Utils;

class MainCardManagementController extends Controller
{


    private $Utils ;
    public function __construct()
    {
        $this->Utils = new Utils();
    }

    public function  index(Request $request){
        try{
            if ($request->isMethod('get')) {
                $deckName = $request->input( 'd_' );
                $description = $request->input( 'description' );
                $status = $request->input( 'status' );
                if($status == "new"){
                    DB::beginTransaction();
                    $table = deck_name::firstOrNew(['deck_name' => $deckName]);
                    if ($table->exists) {
                        // user already exists
                    } else {
                        $table->deck_name =$deckName;
                        $table->deck_description =$description;
                        $table->save();
                    }
                    DB::commit();
                }
                //get fonts
                $fonts = font::all();
                //get frames
                $frames = card_frame::all();
                return view('pages.main_manage_card')
                    ->with('fonts', $fonts)
                    ->with('frames', $frames)
                    ->with('deckname', $deckName);
            }else{
                $response = array(
                    'status' => 'error',
                    'msg' => "Bad Request"
                );
                return response()->json($response);
            }
        }catch (\Exception $e){
            return $e->getTrace();
        }

    }


}