<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class card_frame extends Model
{
    //
    public $timestamps = false;
    protected $table = 'card_frame';
}
