<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class deck_name extends Model
{
    public $timestamps = false;
    protected $table = 'deck_name';
    protected $fillable = ['deck_name, deck_description'];


}
