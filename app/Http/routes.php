<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::get('/', function () {
    return view('pages.welcome');
});
// view card
Route::get('view-card/',[
    'as' => 'view-card',
    'uses' => 'ViewCardController@index'
]);
Route::get('/view-card/get_decks/', 'ViewCardController@getDecks');
Route::post('/view-card/drop_decks/', 'ViewCardController@dropDeck');


// setting
Route::get('setting-app/',[
    'as' => 'setting-app',
    'uses' => 'SettingController@index'
]);
Route::post('/upload.resources', [
    'as' => 'setting.upload-resources',
    'uses' => 'SettingController@storeResources'
]);

// manage-card
Route::get('manage-card/{deck_name}',[
    'as' => 'manage-card',
    'uses' => 'CardManagementController@index'
]);
Route::get('validate-deck/{deckname}', 'CardManagementController@validate_deck');

Route::get('/manage-card/edit/{id}', 'CardManagementController@continue_manage_card');
Route::post('/manage-card/add/', 'CardManagementController@add_card');
Route::post('/manage-card/edit_card/', 'CardManagementController@edit_card');
Route::post('/manage-card/delete/','CardManagementController@delete_card');
Route::get('/manage-card/get_card_deckname/{deckname}', 'CardManagementController@get_card_deckname');
Route::get('/manage-card/get_card_id/{card_id}', 'CardManagementController@get_card_id');

Route::get('main-manage-card/{deck_name}',[
    'as' => 'main-manage-card',
    'uses' => 'MainCardManagementController@index'
]);

// development part
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
    return view('pages.welcome');
    // return what you want
});

Route::get('/download-saved-work/{id}',[
    'as' => 'download-saved-work',
    'uses' => 'CardManagementController@saveWork'
]);

/*Route::any('{catchall}', function() {
    return Response::view('errors.503');
})->where('catchall', '.*');*/