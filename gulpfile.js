var elixir = require('laravel-elixir');
/**
 * gulp
 *
 */

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    //mix.sass('app.scss');

    // Combine scripts
    mix.scripts([
            '../bower/jquery/dist/jquery.js',
            '../bower/bootstrap/dist/js/bootstrap.js',
            '../bower/fabric.js/dist/fabric.min.js',

        ],
        'public/assets/js/admin.js'
    );

    // Compile Less
    mix.less('../less/app.less', 'public/assets/css/admin.css');
});
