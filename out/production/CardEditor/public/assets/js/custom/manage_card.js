/**
 * Created by ICT21 on 12/16/2016.
 */


function constructor(){
    fabric.Object.prototype.toObject = (function (toObject) {
        return function () {
            return fabric.util.object.extend(toObject.call(this), {
                text: this.text,
                obj_type: this.obj_type ,
                obj_id: this.obj_id ,
                obj_order: this.obj_order,
                fontFamily: this.fontFamily,
                selectable: this.selectable,
            });
        };
    })(fabric.Object.prototype.toObject);

    fabric.Canvas.prototype.getItemByAttr = function(attr, name) {
        var object = null,
            objects = this.getObjects();
        for (var i = 0, len = this.size(); i < len; i++) {
            if (objects[i][attr] && objects[i][attr] === name) {
                object = objects[i];
                break;
            }
        }
        return object;
    };
}


function addText(font_name,containerLayers){
    var canvas =  getCanvas(current_canvas);
    $("li").removeClass("actived");
    canvas.deactivateAll();
    var new_text = new fabric.IText('Tap and Type!', {
        fontFamily: font_name,
        top: 100,
        left: 100,
    });
    new_text.on('changed', function(e) {
        new_text.text =new_text.getText();
        console.log('changed', new_text);
    });
    var id  = getUniqueID();
    new_text.text = "Tap and Type! "+id;
    new_text.obj_type ="text";
    new_text.obj_order = id;
    new_text.obj_id = containerLayers + "_"+id;

    objectArray.push(new_text);
    canvas.add(new_text);
    canvas.setActiveObject(new_text);
    canvas.renderAll();
    console.log("Test "+new_text + " " + new_text.toObject());
    // layer
    addLayers(canvas, new_text, "text",containerLayers, containerLayers+"_"+id);
   // console.log(containerLayers + " " + JSON.stringify(canvas));
}

function addImageByURI(image_URI,containerLayers) {
    var reader = new FileReader();
    var canvas = getCanvas(current_canvas);
    $("li").removeClass("actived");
    canvas.deactivateAll();

    reader.onload = function (f) {
        var data = f.target.result;
        fabric.Image.fromURL(data, function (img) {

            var oImg = img.set({
                left: 0,
                top: 0,
                angle: 00,
                width: canvas.getWidth(),
                height: canvas.getHeight()
            });
          /*  oImg.toObject = (function (toObject) {
                return function () {
                    return fabric.util.object.extend(toObject.call(this), {
                        name: this.name,
                        obj_type: "image" ,
                        id: "",
                        order: "",
                    });
                };
            })(oImg.toObject);*/

            var id = getUniqueID() ;
            oImg.text = image_URI.name;
            oImg.obj_type= "image";
            oImg.obj_order = id ;
            oImg.obj_id  = containerLayers+"_"+id ;
            objectArray.push(oImg);
            canvas.add(oImg);


          //  oImg.sendToBack();

            // manage imgae

            curent_frontCard_image = canvas.setActiveObject(oImg);
            canvas.renderAll();

            addLayers(canvas ,oImg, "image",containerLayers, containerLayers+"_"+id);
          //  console.log(containerLayers+" "+ image_URI.name);
        });
    };
    reader.readAsDataURL(image_URI);
}


function addFrame(frame_url, frame_name ,containerLayers){
    var canvas = getCanvas(current_canvas);
    $("li").removeClass("actived");
    canvas.deactivateAll();
    fabric.Image.fromURL(frame_url, function (img) {
        var frame = img.set({left: 0, top: 0,
            angle: 00,
            width: canvas.getWidth(),
            height: canvas.getHeight(),
            selectable: false

        });
        var id = getUniqueID() ;
        frame.obj_type ="frame";
        frame.text = frame_name;
        frame.obj_order = id;
        frame.obj_id  = containerLayers+"_"+id;
        objectArray.push(frame);
        canvas.add(frame);
        console.log("frame "+frame.toObject)
        //   manage Frame ;
        curent_frontCard_frame = canvas.setActiveObject(frame);
        canvas.renderAll();

        addLayers(canvas ,frame, "frame" ,containerLayers, containerLayers+"_"+id);


        // console.log(containerLayers+" "+JSON.stringify(canvas))
    });
}

function manageFrame_Image() {
    var canvas =  getCanvas(current_canvas);
    if(current_canvas == "front_card"){
        alert(curent_frontCard_frame +" "+curent_frontCard_image)
        if(curent_frontCard_frame != null && curent_frontCard_image  != null){
            alert("curent_frontCard_frame group curent_frontCard_image")
            var group = new fabric.Group([curent_frontCard_frame, curent_frontCard_image], {
            });
            canvas.clear().renderAll();
            canvas.add(group).renderAll();
        }
    }else if(current_canvas == "back_card"){
        if(curent_backCard_frame!= null && curent_backCard_image  != null){
            curent_backCard_image.globalCompositeOperation = 'source-atop';
        }
    }
    canvas.renderAll.bind(canvas)
}

function getCanvas(canvas_name) {
    if (canvas_name == "front_card") {
        return frontCard_canvas;
    } else {
        return backCard_canvas;
    }
}

function addLayers(canvas ,elementType, name,containerLayers , id) {

    var li_id = id;
    $("#"+containerLayers).prepend('<li id="'+li_id +'" class="layers ui-state-default actived"> '+ id+"\n"+name +'</li>');
    $("#"+li_id).click(function (evt) {
        if ($(this).hasClass("actived")) {
            // remove active state of all layers and objects
            $("li").removeClass("actived");
            canvas.deactivateAll();
            canvas.renderAll();
        }
        else {
            // remove active state of all layers and objects
            $("li").removeClass("actived");
            canvas.deactivateAll();
            canvas.renderAll();
            // activate layer and object
            $(this).addClass("actived");

          var obj =canvas.getItemByAttr('obj_id', li_id)
            console.log("Select "+obj.obj_id+" "+obj.text)
            canvas.setActiveObject(obj);
            canvas.renderAll();
        }
    });

    elementType.on('selected', function() {
        $("li").removeClass("actived");
        $("#"+li_id).addClass("actived");
    });
}



function reload_page(clear_all) {


    if(clear_all == "false"){

        $("#frontLayers").empty();
        $("#backLayers").empty();
        objectArray = [];
        frontCard_canvas.clear();
        backCard_canvas.clear();

        $("#Card_name").val("");
        $("#Card_comment").val("");
    }else{

        $("#frontLayers").empty();
        $("#backLayers").empty();
        objectArray = [];
        frontCard_canvas.clear();
        backCard_canvas.clear();
        cardListTable.ajax.reload();
        $("#Card_name").val("");
        $("#Card_comment").val("");
    }

}


function save_work() {

}

// select all objects
function deleteObjects(){
    var canvas = getCanvas(current_canvas);
    var activeObject = canvas.getActiveObject();


    if (activeObject) {
        $('#'+activeObject.obj_id).remove()
        canvas.remove(activeObject);
        notification_ok("Delete :"+activeObject.obj_id +" "+activeObject.text);
    }else{
        notification_error("Nothing selected");
    }

}

function getUniqueID(){
    var today = formatDate(new Date());
    return today;
}
function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function formatDate(date) {
    /*var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = addZero(date.getSeconds());
    var strTime = hours + '' + minutes+''+seconds;
    return  date.getFullYear()+date.getMonth()+1 + "" + date.getDate() + "" + +strTime ;*/

    var utc_timestamp = Math.floor(Date.now() / 1000);
    return utc_timestamp;
}