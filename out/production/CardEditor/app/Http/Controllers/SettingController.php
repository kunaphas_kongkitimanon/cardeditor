<?php
/**
 * Created by IntelliJ IDEA.
 * User: Kunaphas
 * Date: 12/12/2016
 * Time: 9:56 PM
 */

namespace App\Http\Controllers;

use App\Http\Model\card_frame;
use App\Http\Model\font;
use Exception;
use Faker\Provider\File;
use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Contracts\Filesystem\Factory as Storage;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Exception\NotReadableException;
use Intervention\Image\Facades\Image;

class SettingController extends Controller
{


    public function __construct()
    {

    }

    public function  index(){

        return view('pages.setting');
    }



    /**
     * @param Storage $storage
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|string
     */
    public function storeResources( Storage $storage, Request $request )
    {
        try {


            if ($request->isXmlHttpRequest() && $request->hasFile('file')) {

                $image = $request->file('file');
                $extension = $image->getClientOriginalExtension();
                if ($extension == 'ttf' || $extension == 'eot' || $extension == 'svg' || $extension == 'woff' || $extension == 'otf') {
                    $type = "FontFiles";

                } else {
                    $type = "FrameFiles";

                }
                $current_time = $this->getTimestamp();
                $timestamp = $this->getFormattedTimestamp($current_time);
                $savedImageName = $this->getSavedImageName($timestamp, $image);
                $savedThumbImageName = $this->getSavedThumbImageName($timestamp, $image);
                $imageUploaded = $this->uploadImage($image, $savedImageName, $storage, $type, $savedThumbImageName);

                if ($imageUploaded) {
                    DB::beginTransaction();
                    // store it on database
                    if ($type == "FontFiles") {
                        $font_name = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
                        //$this->createFontCSS($savedImageName, $storage ,$type,$font_name);
                        $table = new font();
                        $table->font_name = $font_name;
                        $table->font_path = '/data_resources/fonts/' . $savedImageName;
                        $table->upload_time = $current_time;
                        $table->save();
                    } else {
                        $table = new card_frame();
                        $table->frame_name = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
                        $table->frame_path = '/data_resources/frames/' . $savedImageName;
                        $table->frame_thumbnail = '/data_resources/frames/' . $savedThumbImageName;
                        $table->upload_time = $current_time;
                        $table->save();
                    }
                    DB::commit();
                    $data = [
                        'status' => "success",
                        'filename' => $savedImageName
                    ];
                    return json_encode($data, JSON_UNESCAPED_SLASHES);
                }
                $data = [
                    'status' => "error",
                    'filename' => "uploading failed"
                ];

                return json_encode($data, JSON_UNESCAPED_SLASHES);
            } else {

                $data = [
                    'status' => "error",
                    'filename' => "Submit nothing"
                ];
                return json_encode($data, JSON_UNESCAPED_SLASHES);
            }
        }catch (Exception $e){
            DB::rollBack();

            $data = [
                'status' => "server error",
                'filename' => $e->getMessage()
            ];
            return json_encode($data, JSON_UNESCAPED_SLASHES);
        }

    }

    /**
     * @param $image
     * @param $imageFullName
     * @param $storage
     * @param $type
     * @param null $savedThumbImageName
     * @return mixed
     */
    public function uploadImage( $image, $imageFullName, $storage ,$type ,$savedThumbImageName= null)
    {
        $filesystem = new Filesystem;
        $flag = $storage->disk($type)->put( $imageFullName, $filesystem->get( $image ));

        if($type != "FontFiles" && $flag){
            // create Thumbnail
            try{
                $imageRealPath 	=  url('/data_resources/frames/'.$imageFullName);
                $img = Image::make($imageRealPath)->resize(100, 128);

            }
            catch (Exception $e){
                $img = Image::make(public_path() . '/data_resources/frames/'.$imageFullName)->resize(64, 64);
            }
            $img->save(public_path('/data_resources/frames/') . $savedThumbImageName);
        }

        if($flag == false){
            $storage->disk($type)->delete($imageFullName , $savedThumbImageName);
        }

        return $flag;
    }

    /**
     * @return string
     */
    protected function getFormattedTimestamp($time)
    {
        return str_replace( [' ', ':'], '-',$time->toDateTimeString() );
    }

    /**
     * @return string
     */
    protected function getTimestamp()
    {
        return  Carbon::now();
    }

    /**
     * @param $timestamp
     * @param $image
     * @return string
     */
    protected function getSavedImageName( $timestamp, $image )
    {
        return $timestamp . '_' . $image->getClientOriginalName();
    }

    protected function getSavedThumbImageName( $timestamp, $image )
    {
        return $timestamp . '_' ."thumbnail". '_' . $image->getClientOriginalName();
    }

    protected function createFontCSS($imageFullName, $storage ,$type,$font_name){
        $Contents = " @font-face {
            font-family: '".$font_name."' ;
            src:  url('{{ asset('/data_resources/fonts/".$imageFullName."') }}')
        }";
        return $storage->disk($type)->put($imageFullName.'.css',  $Contents);
    }

}