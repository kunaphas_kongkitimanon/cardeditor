<?php
/**
 * Created by IntelliJ IDEA.
 * User: Kunaphas
 * Date: 11/21/2016
 * Time: 3:22 PM
 */

namespace App\Http\Controllers;
use App\Http\Model\Card;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Http\Model\card_frame;
use App\Http\Model\font;
use Illuminate\Http\Request;
use App\Http\Utils;
use App\Http\Model\deck_name;
class CardManagementController extends Controller
{
    private $Utils ;
    public function __construct()
    {
        $this->Utils = new Utils();
    }

    public function  index(Request $request){
        try{
            if ($request->isMethod('post')) {
                $deckName = $request->input( 'deckname' );
                $description = $request->input( 'description' );
                $status = $request->input( 'status' );
                if($status == "new"){
                    DB::beginTransaction();
                    $table = deck_name::firstOrNew(['deck_name' => $deckName]);
                    if ($table->exists) {
                        // user already exists
                    } else {
                        $table->deck_name =$deckName;
                        $table->deck_description =$description;
                        $table->save();
                    }
                    DB::commit();
                }
                //get fonts
                $fonts = font::all();
                //get frames
                $frames = card_frame::all();
                return view('pages.manage_card')
                    ->with('fonts', $fonts)
                    ->with('frames', $frames)
                    ->with('deckname', $deckName);
            }else{
                $response = array(
                    'status' => 'error',
                    'msg' => "Bad Request"
                );
                return response()->json($response);
            }
        }catch (\Exception $e){
            return $e->getTrace();
        }
    }



    public  function  add_card(Request $request){
        try{
            if ($request->isMethod('post')){
                $deckName = $request->input( 'deckname' );
                $card_name = $request->input( 'card_name' );
                $card_description = $request->input( 'card_description' );
                $front_card = $this->Utils->string_compress($request->input( 'front_card' ));
                $back_card =  $this->Utils->string_compress($request->input( 'back_card' ));
                DB::beginTransaction();
                $table = new Card();
                $table->card_name = $card_name;
                $table->card_description = $card_description;
                $table->deck_name = $deckName;
                $table->front_card = $front_card;
                $table->back_card = $back_card;
                $table->save();
                DB::commit();
                $response = array(
                    'status' => 'success',
                    'msg' => 'Card created successfully',
                );
            }else{
                $response = array(
                    'status' => 'BadRequest',
                    'msg' => 'Request not allowed',
                );
            }

            return response()->json($response);

        }catch (\Exception $e){
            $response = array(
                'status' => 'error',
                'msg' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
            );

            return response()->json($response);

        }
    }

    public  function  delete_card(Request $request){
        try{
            if ($request->isMethod('post')){
                $card_id = $request->input( 'card_id' );

                DB::beginTransaction();
                $cards = Card::where('card_id', $card_id)
                    ->delete();
                DB::commit();
                $response = array(
                    'status' => 'success',
                    'msg' => 'Card deleted successfully',
                );
            }else{
                $response = array(
                    'status' => 'BadRequest',
                    'msg' => 'Request not allowed',
                );
            }

            return response()->json($response);

        }catch (\Exception $e){
            $response = array(
                'status' => 'error',
                'msg' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
            );
            return response()->json($response);

        }
    }

    public function  get_card_deckname($deckname){
        $cards = Card::where('deck_name', $deckname)
                ->select('card_id', 'card_name','card_description')
                ->get();
        return response()->json($cards);
    }

    public function  get_card_id($id){
        $cards = Card::where('card_id', $id)
            ->select('front_card', 'back_card')
            ->first();
        $response = array(
            'status' => 'success',
            'front_card' => $this->Utils->string_decompress($cards->front_card),
            'back_card' => $this->Utils->string_decompress($cards->back_card),
        );
        return response()->json($response);
    }

    public function  validate_deck($deckname){
        $deck = deck_name::where('deck_name',$deckname)->first();
        if(count($deck) == 0) {
            $response = array(
                'status' => 'success',
            );
        } else{
            $response = array(
                'status' => 'duplicated',
            );
        }

        return response()->json($response);
    }
}