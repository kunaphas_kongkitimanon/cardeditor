<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    //
    public $timestamps = false;
    protected $table = 'card';
}
