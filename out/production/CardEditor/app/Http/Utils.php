<?php
/**
 * Created by IntelliJ IDEA.
 * User: Kunaphas
 * Date: 12/22/2016
 * Time: 11:49 PM
 */

namespace app\Http;


class Utils
{

    function string_compress($string ){
        $compressed = gzcompress($string, 9);
        return $compressed;
    }

    function string_decompress($string){
      return   gzuncompress($string);
    }
}