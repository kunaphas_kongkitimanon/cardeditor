

@extends('layouts.default')

@section('more_head')
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ URL::asset('assets/css/dropzone.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/amaran.min.css') }}">
    <script src="{{ asset('assets/js/dropzone.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.amaran.min.js') }}"></script>

    <style>

        .dropzone {
            border: 2px dashed #EB7260;
            box-shadow: 0 0 0 5px #373b44;
            padding: 20px;
            background: #373b44;
            color: #fff;
            text-align: center;
        }

        .dropzone h3 {
            color: white;
            text-align: center;
            line-height: 3em;
        }

        .dz-clickable {
            cursor: pointer;
        }

        .dz-drag-hover {
            border: 2px solid #EB7260;
        }

        .dz-preview, .dz-processing, .dz-image-preview, .dz-success, .dz-complete {
            background-color: rgba(0, 0, 0, .5);
            padding: 5px;
        }

        .dz-preview {
            width: auto !important;
        }

        .dz-image {
            text-align: center;
            /*border: 1px solid #EB7260;*/
        }

        .dz-details {
            padding: 10px;
        }

        .dz-success-mark, .dz-error-mark {
            display: none;
        }
        .dz-error-mark{
            color: red;
        }
    </style>

    <script>
         Dropzone.autoDiscover = false;
        $( document ).ready(function() {

        // Disable auto discover for all elements:
            var myDropzone = $('form#myAwesomeDropzone').dropzone({
                paramName           :       "file",
                maxFilesize         :       3, // MB
                dictDefaultMessage  :       "Drop File here or Click to upload Image",
                thumbnailWidth      :       "300",
                thumbnailHeight     :       "300",
                clickable           :       true,
                accept              :       function(file, done) { done() },
                success             :       uploadSuccess,
                complete            :       uploadCompleted,
                init: function(){
                    var _this = this;
                    _this.on('queuecomplete', function(){
                        setTimeout(function(){
                            _this.removeAllFiles();
                        },3000);
                    })
                },
                });


            function uploadSuccess(data, file) {

                var messageContainer    =   $('.dz-success-mark'),
                    message             =   $('<p></p>', {
                        'text' : 'Upload Successfully! '
                    });
                message.appendTo(messageContainer);
                messageContainer.addClass('show');
                $.amaran({
                    'theme'     :'awesome ok',
                    'content'   :{
                        title:'Your upload is Successful!',
                        message:JSON.parse(file).filename,
                        icon:'fa fa-download'
                    },
                    'position'  :'top right',
                    'outEffect' :'slideBottom'
                });
            }

            function uploadCompleted(data) {
                if(data.status != "success")
                {
                    var error_message   =   $('.dz-error-mark'),
                        message         =   $('<p></p>', {
                            'text' : 'Upload Failed'
                        });
                    message.appendTo(error_message);
                    error_message.addClass('show');
                    return;
                }

            }

        });
    </script>
@stop

@section('content')
    <div class="row">
        <center><h1>Config Application</h1></center>
        <p> <h3>1) Install Fonts and Frames</h3></p>
        {!! Form::open([ 'route' => [ 'setting.upload-resources' ], 'files' => true, 'enctype' => 'multipart/form-data', 'class' => 'dropzone', 'id' => 'myAwesomeDropzone' ]) !!}
        <div>
            <h3></h3>
        </div>
        {!! Form::close() !!}
    </div>
@stop
