<style>
    footer {
        position:absolute;
        bottom:0;
        width:80%;
        height:60px;   /* Height of the footer */
        background:transparent;
    }

    p.copyright {
        position: absolute;
        width: 100%;
        color: #fff;
        line-height: 40px;
        font-size: 0.7em;
        text-align: center;
        bottom:0;
    }
</style>

<p class="copyright" ></p>