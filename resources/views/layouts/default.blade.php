
<!DOCTYPE html>
<html>
<head>
    @include('includes.head')
    @yield('more_head')
</head>
<body>
<div class="container">
        @yield('more_header')


    <div class="content">
        @yield('content')
    </div>

    <footer class="row">
        @include('includes.footer')
    </footer>

</div>
</body>
</html>
