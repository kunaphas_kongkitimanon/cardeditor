@extends('layouts.full_width')

@section('more_head')
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/amaran.min.css') }}">
    <script src="{{ asset('assets/js/jquery.amaran.min.js') }}"></script>
    <script src="{{ asset('/assets/js/jquery.backstretch.min.js') }}"></script>
    <script>

        $( document ).ready(function() {
            console.log( "ready!" );
            $.backstretch("https://s-media-cache-ak0.pinimg.com/originals/98/f0/d5/98f0d56de8b25597dddfc2b70ab7f6f7.jpg");

            $('#modal-form-submit').on('click', function(e){
                $.amaran({
                    content:{
                        title:'Please wait!',
                        message:'check existing name',
                        info:'',
                        icon:'fa fa-spinner fa-spin'
                    },
                    sticky :true,
                    theme:'awesome ok'
                });
                e.preventDefault();
                $.ajax({
                    url: '{{ url('/validate-deck/') }}'+"/"+$('#deckname').val(), //this is the submit URL
                    type: 'get', //or POST
                    success: function(data){
                        if(data['status'] == "success"){
                            var url = '{{ url('/manage-card/') }}'
                            var csrf_token = '{{ csrf_token() }}';
                            $("form").attr('action', '{{ url('/manage-card/') }}').attr('method', 'post').submit();
                            $('<form method="post" action='+url+'> ' +
                                '<input type="hidden" name="deckname" value='+$('#deckname').val()+'>' +
                                '<input type="hidden" name="_token" value='+csrf_token+'>' +
                                '<input type="hidden" name="description" value='+$('#description').val()+'>' +
                                '<input type="hidden" name="status" value="new">' +
                                '</form>').appendTo('body')
                                .submit();
                        }else {
                            $.amaran({
                                'clearAll'      :true,
                                'theme'     :'user red',
                                'content'   :{
                                    img:"{{ asset('/assets/images/n02.png') }}",
                                    user:'System',
                                    message:'Duplicate name , try another name !!'
                                },
                                'position'  :'bottom right',
                                'outEffect' :'slideBottom',
                                delay: 5000,
                            });
                        }

                    }
                });


            });
        });
    </script>
    <style>
        .container {
            text-align: center;
            vertical-align: middle;
            display: table-cell;
        }

        .content {
            text-align: center;
            display: inline-block;
            vertical-align: middle;
        }
        body {
            margin: 0;
            padding: 0;
            width: 100%;
            display: table;
            font-weight: 100;

        }
        .modal-content {
            -webkit-border-radius: 0;
            -webkit-background-clip: padding-box;
            -moz-border-radius: 0;
            -moz-background-clip: padding;
            border-radius: 6px;
            background-clip: padding-box;
            -webkit-box-shadow: 0 0 40px rgba(0,0,0,.5);
            -moz-box-shadow: 0 0 40px rgba(0,0,0,.5);
            box-shadow: 0 0 40px rgba(0,0,0,.5);
            color: #000;
            background-color: #fff;
            border: rgba(0,0,0,0);
        }
        .glyphicon {
            font-size: 25px;
        }
        .icon-green {
            color: #53a93f;
        }

        .icon-blue{
            color: #57b5e3;
        }

        .icon-red {
            color: #d73d32;
        }

        .icon-yellow {
            color: #f4b400;
        }


    </style>
@stop

@section('content')
    <div class="col-md-6 text-center">
        <a href="#createDeckModal" type="button" data-toggle="modal" rel="tooltip" class="btn btn-default">Create New Card</a>
        <a href="{{ route('view-card') }}" type="button" class="btn btn-default">View Card</a>
        <a href="{{ route('setting-app') }}" type="button" class="btn btn-default">Setting</a>
    </div>

    <div class="modal fade" id="createDeckModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <i class="glyphicon glyphicon-fire icon-yellow"></i><i class="glyphicon glyphicon-fire icon-red"></i>
                    <i class="glyphicon glyphicon-fire icon-blue"></i><i class="glyphicon glyphicon-fire icon-green"></i>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel"> Create new Deck</h4>
                </div>
                <div class="modal-body">

                        <div class="form-group">
                            <label for="recipient-name" class="form-control-label"> Name:</label>
                            <input type="text" class="form-control"  name="deckname" id="deckname">
                            <input  class="form-control" name="status" id="status" value="new" type="hidden">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="form-control-label">Description:</label>
                            <textarea class="form-control" name="description" id="description"></textarea>
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="modal-form-submit" type="button" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
@stop