
@extends('layouts.default')

@section('more_head')
    <link rel="stylesheet" href="{{ URL::asset('assets/css/custom.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap-select.css') }}" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.1/css/select.bootstrap.min.css" />
    <link href="{{ URL::asset('assets/css/loading-bar.css') }}" rel="stylesheet" />


    <link rel="stylesheet" href="{{ URL::asset('assets/css/shepherd/shepherd-theme-arrows.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('assets/css/shepherd/shepherd-theme-arrows-fix.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('assets/css/shepherd/shepherd-theme-arrows-plain-buttons.css') }}" />


    <script src="{{ asset('assets/js/bootstrap-select.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/js/FileSaver.js') }}"></script>
    <script src="{{ asset('assets/js/canvas-toBlob.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap-modal-popover.js') }}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js" ></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="{{ asset('assets/js/pace.min.js') }}"></script>
    <!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs-3.3.7/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.13/af-2.1.3/b-1.2.4/b-colvis-1.2.4/b-flash-1.2.4/b-html5-1.2.4/b-print-1.2.4/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.2.0/r-2.1.0/rr-1.2.0/sc-1.4.2/se-1.2.0/datatables.min.css"/>
      <script type="text/javascript" src="https://cdn.datatables.net/v/bs-3.3.7/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.13/af-2.1.3/b-1.2.4/b-colvis-1.2.4/b-flash-1.2.4/b-html5-1.2.4/b-print-1.2.4/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.2.0/r-2.1.0/rr-1.2.0/sc-1.4.2/se-1.2.0/datatables.min.js"></script>
  -->
    <script src="{{ asset('assets/js/shepherd/tether.js') }}"></script>
    <script src="{{ asset('assets/js/shepherd/shepherd.min.js') }}"></script>

    <script src="{{ asset('assets/js/custom/notificationManager.js') }}"></script>
    <script src="{{ asset('assets/js/custom/manage_card.js') }}"></script>
    <style type="text/css">
        @foreach ($fonts as $font)
         @font-face {
            font-family: '{{ $font->font_name}}' ;
            src:  url('{{ asset($font->font_path) }}')
        }
        @endforeach

        .highlight {
            border: solid blue;
        }
        .btn-default.btn-on.active{background-color: #5BB75B;color: white;}
        .btn-default.btn-off.active{background-color: #5BB75B;color: white;}

        .canvas-size{
            border:1px solid black;
            width: 100%;
            height: auto;
            z-index: 10;
        }

        .layers.li{
            margin: 0 3px 3px 3px;
            border: 1px solid black;
        }
        .actived{
            border: 1px solid red;
        }

        .btn-circle {
            width: 30px;
            height: 30px;
            text-align: center;
            padding: 6px 0;
            font-size: 12px;
            line-height: 1.428571429;
            border-radius: 15px;
            z-index:2; position:absolute;
            top: 1px;
        }

        body > :not(.pace),body:before,body:after {
            -webkit-transition:opacity .4s ease-in-out;
            -moz-transition:opacity .4s ease-in-out;
            -o-transition:opacity .4s ease-in-out;
            -ms-transition:opacity .4s ease-in-out;
            transition:opacity .4s ease-in-out
        }

        body:not(.pace-done) > :not(.pace),body:not(.pace-done):before,body:not(.pace-done):after {
            opacity:0
        }
    </style>

    <script>
        var deck_name ='{{ $deckname }}';
        var current_canvas = "front_card";
        var curent_frontCard_frame ="";
        var curent_frontCard_image ="";
        var curent_backCard_frame ="";
        var curent_backCard_image ="";
        var current_card_id="";

        var frontCard_canvas;
        var backCard_canvas;
        // Bootstrap tooltip
        var FrontobjectArray = new Array();
        var BackobjectArray = new Array();
        var containerLayers = "frontLayers";
        var cardListTable ;
        var currentStatusCard = "new";
        var tour;

        $(window).bind("load", function() {
            @foreach ($fonts as $font)
               $('{{ "#". $font->font_name}}').css('font-family', '{{  $font->font_name }}' );
            @endforeach
        });

        $(function () {
            // set tour
            tour = new Shepherd.Tour({
                defaults: {
                    classes: 'shepherd-theme-arrows',
                    scrollTo: true
                }
            });
            tour.addStep('example-switch', {
                text: 'เลือกเพื่อที่จะปรับเเต่ง Card หน้าหลัง ',
                attachTo: '.guide1 bottom',
                classes: 'shepherd shepherd-open shepherd-theme-arrows shepherd-transparent-text',
                buttons: [
                    {

                        text: 'Next',
                        action: tour.next
                    }
                ]
            });




            $("#cardStatus").html( 'Card Status :  New <i class="fa fa-cubes text-success" aria-hidden="true"></i>');
            $.ajaxSetup(
                {
                    headers:
                        {
                            'X-CSRF-Token': $('input[name="_token"]').val()
                        }
                });
            cardListTable = $('#cardList').DataTable( {
                "ajax": {
                    "url":  '{{ url('/manage-card/get_card_deckname/') }}'+"/"+deck_name,
                    "dataSrc": ""
                },
                "columns": [
                    { "data": "card_name" },
                    { "data": "card_description" },
                    {
                        "data": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            return '<button type="button"  onclick="delete_card(\''+ o.card_id +'\')" class="btn btn-delete">delete</button> '+
                                '<button type="button"  onclick="edit_card(\''+o.card_id+'\')"  class="btn btn-delete">edit</button> ';
                        }
                    }
                ]
            } );


            frontCard_canvas = new fabric.Canvas('front_card');
            frontCard_canvas.CustomObject = {};
            frontCard_canvas.on('mouse:move', function(options) {
                console.log(options.e.layerX, options.e.layerY);
            });

            backCard_canvas = new fabric.Canvas('back_card');



            $('.collapse').on('show.bs.collapse', function() {
                var id = $(this).attr('id');
               // notification_ok(id);
                if (id == "faq-cat-1-sub-1") {
                    $('input:radio[name=multifeatured_module][value=front_card]').click(); ;
                } else {
                    $("input:radio[name=multifeatured_module][value=back_card]").click();
                }
                $('a[href="#' + id + '"]').closest('.panel-heading').addClass('active-faq');
                $('a[href="#' + id + '"] .panel-title span').html('<i class="glyphicon glyphicon-minus"></i>');
            });
            $('.collapse').on('hide.bs.collapse', function() {
                var id = $(this).attr('id');
                $('a[href="#' + id + '"]').closest('.panel-heading').removeClass('active-faq');
                $('a[href="#' + id + '"] .panel-title span').html('<i class="glyphicon glyphicon-plus"></i>');
            });

            var toggle = $('#ss_toggle');
            var menu = $('#ss_menu');
            var rot ;
            $('#ss_toggle').on('click', function (ev) {
                rot = parseInt($(this).data('rot')) - 180;
                menu.css('transform', 'rotate(' + rot + 'deg)');
                menu.css('webkitTransform', 'rotate(' + rot + 'deg)');
                if ((rot / 180) % 2 == 0) {
                    //Moving in
                    toggle.parent().addClass('ss_active');
                    toggle.addClass('close');
                } else {
                    //Moving Out
                    toggle.parent().removeClass('ss_active');
                    toggle.removeClass('close');
                }
                $(this).data('rot', rot);
            });
            menu.on('transitionend webkitTransitionEnd oTransitionEnd', function () {
                if ((rot / 180) % 2 == 0) {
                    $('#ss_menu div i').addClass('ss_animate');
                } else {
                    $('#ss_menu div i').removeClass('ss_animate');
                }
            });
            // first initialize frontcard
            $("input:radio[name='multifeatured_module']:checked");
            $('#div_front_card').addClass("highlight");


            $("input[name='multifeatured_module']").change(function () {
                var selected_card = $(this).val();
                //alert(selected_card);
                notification_ok(selected_card);
                if (selected_card == "front_card") {
                    focusFrontCard()
                } else {
                    focusBackCard()
                }
            });

            $('ul.switch_card li').click(function(e)
            {
                var selected_card = $(this).text().split(" ");
                notification_ok(selected_card);
                if (selected_card == "Front") {
                    focusFrontCard()

                } else if(selected_card == "Back"){
                    focusBackCard()

                }
            });


            $('#addImage').change(function (e) {
                var image_URI = e.target.files[0];
                addImageByURI(image_URI ,containerLayers);
            });

            $('#btn_addFrame').on('click', function (e) {
                var image_URI = $("#frames  option:selected").val();
                var frame_name = $("#frames  option:selected").text();
                if(frame_name != "Select...") {
                    addFrame(image_URI , frame_name ,containerLayers);
                }else{
                    alert("selectframe")
                }
            });

            $('#btn_addText').on('click', function (ev) {
                var value = $("#fonts  option:selected").val();
                var font_name = $("#fonts  option:selected").text();
                font_name= font_name.replace(/ |\n/g, "");
                if(font_name != "Select..."){
                    addText(font_name,containerLayers);
                }else{
                    alert("select fonts")
                }
            });

            $('.save-card').on('click', function (ev) {
                //    console.log(JSON.stringify(frontCard_canvas));
                //   console.log(JSON.stringify(backCard_canvas));
                add_card($("#Card_name").val()  ,
                    $("#Card_comment").val() ,
                    JSON.stringify(frontCard_canvas.toDatalessJSON())  ,
                    JSON.stringify(backCard_canvas) ,
                    deck_name);
            });

            $('#export-card').on('click', function (ev) {
                //console.log(JSON.stringify(frontCard_canvas));
                $('#front_card').get(0).toBlob(function(blob){
                    saveAs(blob, "front_cardIMG.jpeg");
                });
                $('#back_card').get(0).toBlob(function(blob){
                    saveAs(blob, "back_cardIMG.jpeg");
                });
            });


            $('#new-card').on('click', function (ev) {
                //console.log(JSON.stringify(frontCard_canvas));
                notification_ok("New Card'");
                reload_page(true);
            });

            $('#divDelele').on('click', function (ev) {
                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert deck: "+deck_name,
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function () {
                    drop_deck(deck_name,'{{ url('/view-card/drop_decks/') }}' ,'{{URL::to('/view-card/')}}');
                })
            });
            $('#divSave').on('click', function (ev) {
                ev.preventDefault();  //stop the browser from following


                var link = document.createElement("a");
                $(link).click(function(e) {
                    e.preventDefault();
                    window.location.href = '{{route('download-saved-work', ['deckname' => $deckname])}}'
                });
                $(link).click();
            });
            $('#divDownload').on('click', function (ev) {
                ev.preventDefault();  //stop the browser from following
                notification_ok("Clear "+current_canvas);
                clear_current_canvase();
            });
            $('#divToolTips').on('click', function (ev) {
                tour.start();

            });

            $('.delete-object-icon').on('click', function (e){
               // $('#dropObjConfirmation').modal('show');

                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function () {
                    deleteObjects();
                   /* swal(
                        'Deleted!',
                        'Your card has been deleted.',
                        'success'
                    )*/
                })

            });

            $('#gen-front-card').on('click', function (e) {
                var frame_name = $("#frames  option:selected").text();

                if(frame_name != "Select..."){
                    var font_name = $("#fonts  option:selected").text();
                    var frame_type = getCardType(frame_name);


                    if(font_name == "Select..."  ) {

                        notification_error_withXmark("Select Fonts");
                    }else{

                        var frontCardOBJ = new Object();
                        frontCardOBJ.front_card_nameTH = $("#front_card_nameTH").val();
                        frontCardOBJ.front_card_nameEN = $("#front_card_nameEN").val();
                        frontCardOBJ.front_card_level = $("#front_card_lv").val();
                        frontCardOBJ.front_card_type = $("#front_card_type").val();
                        frontCardOBJ.front_card_clan = $("#front_card_clan").val();
                        frontCardOBJ.front_card_qoute = $("#front_card_qoute").val();
                        frontCardOBJ.front_card_description = $("#front_card_description").val();
                        frontCardOBJ.front_card_creator = $("#front_card_creator").val();
                        frontCardOBJ.attack_left = $("#front_card_attack").val();
                        frontCardOBJ.defense_right = $("#front_card_defense").val();

                        if(frame_type.includes("spirit")){
                            gen_front_card(font_name,frontCardOBJ ,frame_type)
                        }else{
                            gen_front_card_miracle(font_name,frontCardOBJ ,frame_type)
                        }
                        // get all values from front-card input boxes


                    }
                }else{
                    notification_error_withXmark("Select Card Frame");
                }

            });

            constructor();
        });

        function drop_deck(deck_name ,url , home_url){

            $.ajax({
                url: url,
                type: 'post',
                data: {
                    deck_name: deck_name,
                },
                success: function (data) {
                    // console.log(data);
                    if (data['status'] == "success") {
                        notification_ok(data['msg']);
                        window.location.replace(home_url);
                    } else {
                        notification_error_withXmark(data['msg']);
                    }
                },
                error: function (xhr, b, c) {
                    notification_error_withXmark("xhr=" + xhr + " b=" + b + " c=" + c);
                }
            });
        }

        function add_card($card_name ,$card_description ,$front_card ,$back_card , $deck_name) {
            clear_all = "true";
            if(currentStatusCard == "New"){
                var url = '{{ url('/manage-card/add/') }}';
                var data = {
                    card_name: $card_name,
                    card_description: $card_description,
                    front_card: $front_card,
                    back_card: $back_card,
                    deckname: $deck_name,
                };
            }else{
                var url = '{{ url('/manage-card/edit_card/') }}';
                var data = {
                    card_name: $card_name,
                    card_description: $card_description,
                    front_card: $front_card,
                    back_card: $back_card,
                    deckname: $deck_name,
                    card_id: current_card_id,
                };
            }

            $.ajax({
                url: url,
                type: 'post',
                data: data,
                success: function (data) {
                    // console.log(data);

                    if (data['status'] == "success") {
                        notification_ok(data['msg']);
                        reload_page(clear_all);
                    } else {
                        notification_error_withXmark(data['msg']);
                    }
                },
                error: function (xhr, b, c) {
                    notification_error_withXmark("xhr=" + xhr + " b=" + b + " c=" + c);
                }
            });

        }

        function delete_card(card_id) {
            //console.log(JSON.stringify(frontCard_canvas));
            if(current_card_id == card_id){
                clear_all = "true";
            }else{
                clear_all =  "false";
            }
            $.ajax({
                url: '{{ url('/manage-card/delete/') }}',
                type: 'post',
                data: {
                    card_id: card_id,
                },
                success: function (data) {
                    // console.log(data);
                    if (data['status'] == "success") {
                        notification_ok(data['msg']);
                        reload_page(clear_all);
                    } else {
                        notification_error_withXmark(data['msg']);
                    }
                },
                error: function (xhr, b, c) {
                    notification_error_withXmark("xhr=" + xhr + " b=" + b + " c=" + c);
                }
            });

        }

        function edit_card(card_id) {
            current_card_id = card_id;
            $("#cardStatus").html('Card Status : Edit <i class="fa fa-cubes text-warning" aria-hidden="true"></i>');
            currentStatusCard = "edit"
            reload_page("false");
            Pace.track(function() {
                $.ajax({
                    url: '{{ url('/manage-card/get_card_id/') }}'+'/'+card_id,
                    type: 'get',
                    success: function (data) {
                        // console.log(data);
                        if (data['status'] == "success") {
                            notification_ok("Load Complete...");
                            load_card(data);
                        } else {
                            notification_error_withXmark("Nothing return");
                        }
                    },
                    error: function (xhr, b, c) {
                        notification_error_withXmark("xhr=" + xhr + " b=" + b + " c=" + c);
                    }
                });
            });

        }

        function load_card(obj){

            console.log(obj);
            var frontCradtJson = JSON.parse(obj.front_card );

            frontCard_canvas.loadFromJSON(obj.front_card, frontCard_canvas.renderAll.bind(frontCard_canvas), function(o, object){
                var myDate = new Date();
                console.log(object.obj_type +" "+  myDate.getHours() + ":" + myDate.getMinutes() + ":" + myDate.getSeconds() +
                    ":" + myDate.getMilliseconds()  );
                addObjectToArray("front_card" ,object);
                addLayers( frontCard_canvas ,object, object.obj_type, "frontLayers" ,object.obj_id )
               /* if(object.obj_type  == "text"){
                         objectArray.push(object);
                        addLayers( frontCard_canvas ,object, object.obj_type, "frontLayers" ,object.obj_id )
                }
                else if(object.obj_type  == "frame"){

                    objectArray.push(object);
                        //   manage Frame ;
                        addLayers(frontCard_canvas ,object, object.obj_type , "frontLayers", object.obj_id  );

                }else if(object.obj_type  == "image"){
                    objectArray.push(object);
                        //   manage image ;
                        addLayers(frontCard_canvas ,object, object.obj_type  , "frontLayers",object.obj_id  );
                }*/
            });

            backCard_canvas.loadFromJSON(obj.back_card ,backCard_canvas.renderAll.bind(backCard_canvas), function(o, object){
                var myDate = new Date();
                console.log(object.obj_type +" "+  myDate.getHours() + ":" + myDate.getMinutes() + ":" + myDate.getSeconds() +
                    ":" + myDate.getMilliseconds()  );
                addObjectToArray("back_card" ,object);
                addLayers( backCard_canvas ,object, object.obj_type, "backLayers" ,object.obj_id )
            });

            // set text box
            $("#Card_name").val(obj.card_name)
            $("#Card_comment").val(obj.card_description)
        }



    </script>
@stop

<nav class="navbar navbar-default ">

        <div id="tool_list" style="border-top: 10px">
            <div  class="col-md-2">
                <div class="btn-group guide1" id="myToggleButton_card" data-toggle="buttons">
                    <label class="btn btn-default btn-on btn-lg active">
                        <input type="radio" value="front_card" name="multifeatured_module">Front</label>
                    <label class="btn btn-default btn-off btn-lg">
                        <input type="radio" value="back_card" name="multifeatured_module">Back</label>
                </div>
            </div>
            <div  class="col-md-4">
                <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                    <div class="btn-group" role="group" aria-label="First group">
                        <button type="button" class="btn btn-secondary save-card"  id="save-card-0" ><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                    </div>
                    <div class="btn-group" role="group" aria-label="Second group">
                        <button type="button" class="btn btn-secondary" id="export-card"><i class="fa fa-download" aria-hidden="true"></i></button>
                    </div>
                    <div class="btn-group" role="group" aria-label="Third group">
                        <button type="button" class="btn btn-secondary"  id="new-card" ><i class="fa fa-file-text" aria-hidden="true"></i></button>
                    </div>
                    <span class="btn btn-primary btn-file">
                 <i class="fa fa-picture-o" aria-hidden="true"></i><input type='file' id="addImage"  accept="image/*"  />
               </span>
                </div>

            </div>
            <div  class="col-md-3">
                    <label>Frame:</label>
                <div class="input-group">
                    <select title="Select your surfboard"  id="frames" name="frames" class=" form-control selectpicker" data-live-search="true">
                        <option>Select...</option>
                        @foreach($frames as $frame)
                            <option data-thumbnail= "{{url("/").$frame->frame_thumbnail}}" value="{{url("/").$frame->frame_path}}"  >{{$frame->frame_name}}</option>
                        @endforeach
                    </select>
                    <span class="input-group-btn">
                     <button id="btn_addFrame" class="btn btn-default" type="button">Set <span class="fa fa-plus" ></span></button>
                    </span>
                </div>

            </div>
            <div  class="col-md-3">
                <label>Font:</label>
                <div class="input-group">
                    <select id="fonts" name="fonts" class="form-control ">
                        <option>Select...</option>
                        @foreach($fonts as $font)
                            <option    id="{{ $font->font_name }}" value="{{$font->font_id}}"  >
                                {{$font->font_name}}
                            </option>
                        @endforeach
                    </select>
                    <span class="input-group-btn">
                     <button id="btn_addText" class="btn btn-default" type="button" >Set <span class="fa fa-plus" ></span></button>
                    </span>
                </div>
            </div>
        </div>
</nav>

@section('content')

    {{ csrf_field() }}
    <div class="row" style="margin-top: 10px ; margin-bottom: 10px">
        <div id="panel" class="col-md-12">
                <h4><div id="cardStatus"> </div></h4>
                <div class="panel with-nav-tabs panel-success ">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs switch_card">
                            <li class="active" ><a href="#tab1primary" data-toggle="tab">Front</a></li>
                            <li ><a href="#tab2primary" data-toggle="tab">Back</a></li>
                            <li ><a href="#tab3primary" data-toggle="tab">Card Detail</a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab1primary">
                                <div class="col-md-4">
                                     <canvas id="front_card" style="z-index:1" width="361px" height="523px" class="canvas-size" ></canvas>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label  class="control-label">Card name English</label>
                                        <input type="text" class="form-control" value="Raging Frame spirit" id="front_card_nameEN">
                                    </div>
                                    <div class="form-group">
                                        <label  class="control-label" >Card name TH</label>
                                        <input type="text" class="form-control" value="ปีศาจเพลิงโลกัล" id="front_card_nameTH">
                                    </div>
                                    <div cclass="form-group">
                                        <label  class="control-label" >Card Level</label>
                                        <input type="text" class="form-control" value="1" id="front_card_lv">
                                    </div>
                                    <div cclass="form-group">
                                        <label  class="control-label" >Card Attack</label>
                                        <input type="text" class="form-control" value="1" id="front_card_attack">
                                    </div>
                                    <div cclass="form-group">
                                        <label  class="control-label" >Card Defense</label>
                                        <input type="text" class="form-control" value="0" id="front_card_defense">
                                    </div>

                                    <div class="form-group">
                                        <label  class="control-label">Card type</label>
                                        <input type="text" class="form-control" value="สปิริต/ไฟ" id="front_card_type">
                                    </div>
                                    <div class="form-group">
                                        <label  class="control-label" >Card clan</label>
                                        <input type="text" class="form-control" value="เอลฟ์/นักเวทย์" id="front_card_clan" >
                                    </div>
                                    <div class="form-group">
                                        <label  class="control-label" >Card qoute</label>
                                        <input type="text" class="form-control" value="จะขอเผาที่สิ่งให้มอดไหม้" id="front_card_qoute">
                                    </div>
                                    <div class="form-group">
                                        <label  class="control-label" >Card description</label>
                                        <input type="text" class="form-control" value="ส่งการ์ดเวทย์ต่อเนื่อง หรือกับดักต่อเนื่องที่หงายหน้าอยู่บนสนามเรา 1 ใบลงสุสาน. ทำความเสียหายกับผู้เล่นฝ่ายตรงข้าม 500. เอฟเฟคนี้สามารถใช้งานได้มากสุดเทิร์นละ 2 ครั้ง
                                            " id="front_card_description">
                                    </div>
                                    <div class="form-group">
                                        <label  class="control-label" >Card Editor</label>
                                        <input type="text" class="form-control" value="ART: Apollo" id="front_card_editor">
                                    </div>
                                    <br>
                                    <button type="button" class="btn btn-secondary "  id="gen-front-card" >Generate <i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="tab2primary">
                                <div class="col-md-4">
                                <canvas id="back_card"  style="z-index:1" width="361px" height="523px"  class="canvas-size" ></canvas>
                                </div>
                                <div class="col-md-8">
                                    <p>Head</p>
                                    <input type="text" class="form-control" id="Card_name">
                                    <p>Description</p>
                                    <textarea class="form-control" rows="5" id="Card_comment"></textarea>
                                    <br>
                                    <button type="button" class="btn btn-secondary "  id="gen-back-card" >Generate <i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="tab3primary">
                                <div class="col-md-8">
                                    <p>Card name</p>
                                    <input type="text" class="form-control" id="Card_name">
                                    <p>Card description</p>
                                    <textarea class="form-control" rows="5" id="Card_comment"></textarea>
                                    <br>
                                    <button type="button" class="btn btn-secondary "  id="save-card-1" >save <i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h3>Deck Name : {{ $deckname }}</h3>
            <table id="cardList" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Card Name</th>
                    <th>Card Description</th>
                    <th>Manage</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    <div id='ss_menu' style="position:fixed;z-index:1500">
        <div id="divDownload">
            <i class="glyphicon glyphicon-erase"  title="Clear Current Card" ></i>
        </div>
        <div id="divSave">

                <i  class="glyphicon glyphicon-level-up" title="Export work"></i>

        </div>
        <div id="divDelele">
            <i class="glyphicon glyphicon-trash" title="Drop this deck"></i>
        </div>
        <div id="divToolTips" data-target=".bs-example-modal-lg">
            <i  class="glyphicon glyphicon-education" title="ToolTips"></i>
        </div>

        <div class='menu'>
            <div class='share' id='ss_toggle' data-rot='180'>
                <div class='circle'></div>
                <div class='bar'></div>
            </div>
        </div>
    </div>

    <!-- Define content
    <div id="popup_FrontInfo" class="popover">
        <div class="arrow"></div>
        <h3 class="popover-title">Card info</h3>
        <div class="popover-content">
            <p>Card name</p>
            <input type="text" class="form-control" id="Card_name">
            <p>Card description</p>
            <textarea class="form-control" rows="5" id="Card_comment"></textarea>
            <br>
            <a href="#" class="btn btn-default" data-dismiss="modal">ok</a>
        </div>
    </div>-->


@stop
