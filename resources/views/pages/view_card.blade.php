
@extends('layouts.default')

@section('more_head')
    <link rel="stylesheet" href="{{ URL::asset('assets/css/custom.css') }}" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.1/css/select.bootstrap.min.css" />
    <link href="{{ URL::asset('assets/css/corner-indicator.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ URL::asset('assets/css/amaran.min.css') }}">
    <script src="{{ asset('assets/js/jquery.amaran.min.js') }}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js" ></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="{{ asset('assets/js/pace.min.js') }}"></script>

    <!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs-3.3.7/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.13/af-2.1.3/b-1.2.4/b-colvis-1.2.4/b-flash-1.2.4/b-html5-1.2.4/b-print-1.2.4/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.2.0/r-2.1.0/rr-1.2.0/sc-1.4.2/se-1.2.0/datatables.min.css"/>


      <script type="text/javascript" src="https://cdn.datatables.net/v/bs-3.3.7/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.13/af-2.1.3/b-1.2.4/b-colvis-1.2.4/b-flash-1.2.4/b-html5-1.2.4/b-print-1.2.4/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.2.0/r-2.1.0/rr-1.2.0/sc-1.4.2/se-1.2.0/datatables.min.js"></script>
  -->

    <style type="text/css">

        .highlight {
            border: solid blue;
        }
        .btn-default.btn-on.active{background-color: #5BB75B;color: white;}
        .btn-default.btn-off.active{background-color: #5BB75B;color: white;}

        .canvas-size{
            border:1px solid black;
            width: 100%;
            height: auto;
            z-index: 10;
        }

        .layers.li{
            margin: 0 3px 3px 3px;
            border: 1px solid black;
        }
        .actived{
            border: 1px solid red;
        }

        .btn-circle {
            width: 30px;
            height: 30px;
            text-align: center;
            padding: 6px 0;
            font-size: 12px;
            line-height: 1.428571429;
            border-radius: 15px;
            z-index:2; position:absolute;
            top: 1px;
        }
        .modal  {
            /*   display: block;*/
            padding-right: 0px;
            background-color: rgba(4, 4, 4, 0.8);
        }

        .modal-dialog {
            top: 20%;
            width: 100%;
            position: absolute;
        }
        .modal-content {
            border-radius: 0px;
            border: none;
            top: 40%;
        }
        .modal-body-drop {
            background-color: #d5392f;
            color: white;
        }
        .modal-body-save {
            background-color: #2d8831;
            color: white;
        }
    </style>

    <script>
        var deck_name ="test";


        var cardListTable ;

        $(function () {

            $.ajaxSetup(
                {
                    headers:
                        {
                            'X-CSRF-Token': $('input[name="_token"]').val()
                        }
                });
            cardListTable = $('#deckList').DataTable( {
                "ajax": {
                    "url":  '{{ url('/view-card/get_decks') }}',
                    "dataSrc": ""
                },
                "columns": [
                    { "data": "deck_name" },
                    { "data": "deck_description" },
                    { "data": "updated_date" },
                    {
                        "data": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            return '<button type="button"  onclick="toggle_drop_deck(\''+ o.deck_name +'\')" class="btn btn-delete">delete</button> '+
                             '<button type="button"  onclick="toggle_view_deck(\''+ o.deck_name +'\')" class="btn btn-delete">view</button> '+
                             '<button type="button"  onclick="toggle_download_deck(\''+ o.deck_name +'\')" class="btn btn-delete">download</button><br><br> '+
                                '<button type="button" onclick="toggle_main_edit_deck(\''+ o.deck_name +'\')" class="btn btn-delete">edit</button> '+
                                '<button type="button" onclick="toggle_edit_deck(\''+ o.deck_name +'\')" class="btn btn-delete">Freely edit(Beta)</button> ';
                        }
                    }
                ]
            } );
            $('#dropIT').on('click', function (e) {
                drop_deck(deck_name,'{{ url('/view-card/drop_decks/') }}');
            });


        });

        function toggle_drop_deck(name){
            deck_name =name;
            $('#dropConfirmation').modal('toggle');
        }

        function toggle_edit_deck(deck_name) {
          //  $.amaran({'message': "'"+ '{{ url('/manage-card/') }}' +"/"+deck_name+"'"});
            edit_deck(deck_name,'{{ url('/manage-card/') }}' +"/"+deck_name+"'" , '{{ csrf_token() }}');
        }


        function toggle_main_edit_deck(deck_name) {
            //  $.amaran({'message': "'"+ '{{ url('/manage-card/') }}' +"/"+deck_name+"'"});
            edit_deck(deck_name,'{{ url('/main-manage-card/') }}' +"/"+deck_name+"'" , '{{ csrf_token() }}');
        }

    </script>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>Deck list</h3>
            <table id="deckList" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Deck Name</th>
                    <th>Deck Description</th>
                    <th>Last Update</th>
                    <th>Manage</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    <div  id="dropConfirmation" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body-drop">
                    <center> <H2>Drop whole deck?</H2>
                        <h4> <label>
                                ** This action cannot be undone **
                            </label>
                        </h4>
                        <button id="dropIT" type="button" class="btn btn-danger"> drop </button>   <button type="button" data-dismiss="modal" class="btn btn-primary">Cancel</button>
                        <br>   <br>

                    </center>
                </div>
            </div>
        </div>
    </div>


@stop
<script src="{{ asset('assets/js/custom/view_card.js') }}"></script>
<script src="{{ asset('assets/js/custom/notificationManager.js') }}"></script>
