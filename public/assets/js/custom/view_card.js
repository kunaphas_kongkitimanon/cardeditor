/**
 * Created by Kunaphas on 12/27/2016.
 */

function drop_deck(deck_name ,url){
    Pace.track(function() {
        $.ajax({
            url: url,
            type: 'post',
            data: {
                deck_name: deck_name,
            },
            success: function (data) {
                // console.log(data);
                if (data['status'] == "success") {
                    notification_ok(data['msg']);
                    location.reload(true);
                } else {
                    notification_error_withXmark(data['msg']);
                }
            },
            error: function (xhr, b, c) {
                notification_error_withXmark("xhr=" + xhr + " b=" + b + " c=" + c);
            }
        });
    });

}

function edit_deck(deck_name ,url , csrf_token){
    Pace.track(function() {
        $('<form method="get" action='+url+'> ' +
            '<input type="hidden" name="d_" value='+deck_name+'>' +
            '</form>').appendTo('body')
            .submit();
    });

}

