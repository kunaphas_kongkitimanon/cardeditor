/**
 * Created by Kunaphas on 12/27/2016.
 */
function notification_ok(msg ){
    $.amaran({
        'theme'     :'colorful',
        'content'   :{
            bgcolor:'#27ae60',
            color:'#fff',
            message:msg
        },
        'position'  :'bottom left',
        'outEffect' :'slideTop',
        delay: 5000,
    });

}
function notification_error(msg ){
    $.amaran({
        'theme'     :'colorful',
        'content'   :{
            bgcolor:'#ff6961' ,
            color:'#fff',
            message:msg
        },
        'position'  :'top left',
        'outEffect' :'slideBottom',
        delay: 5000,
    });

}

function notification_error_withXmark(msg ){
    $.amaran({
        'theme'     :'colorful',
        'content'   :{
            bgcolor:'#ff6961' ,
            color:'#fff',
            message:msg
        },
        'sticky'        :true,
        'closeOnClick'  :false,
        'closeButton'   :true,
        'position'  :'bottom left',
        'outEffect' :'slideTop',
        delay: 5000,
    });
}
function RandomColor() {
    colors = ['red', 'white', 'blue', 'green','yellow','cyan', 'violet', 'orange',]
    return colors[Math.floor(Math.random()*colors.length)];
}
function notification_by_system(msg) {
    var theme ='user '+RandomColor()
    $.amaran({
        'theme'     : theme,
        'content'   :{
            img:"../public/assets/images/n02.png",
            user:'Bob',
            message:msg
        },
        'position'  :'bottom right',
        'outEffect' :'slideBottom',
        delay: 5000,
    });
}