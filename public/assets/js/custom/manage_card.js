/**
 * Created by ICT21 on 12/16/2016.
 */
function gen_front_card_miracle(font_name ,obj ,type){

    if(type == "01"){

    }else if (type == "02"){

    }else if(type == "03"){

    }else if(type == "04"){

    }else {

    }
    var canvas =  getCanvas("front_card");

    // gen header Eng (72/17)
    var head_EN = new fabric.Text(obj.front_card_nameEN, {
        fontFamily: font_name,
        fontSize: 50,
        top: 10,
        left: 60,
        textAlign: 'center',
        fixedWidth: 200,
        width: 200,
    });

    if (head_EN.width > head_EN.fixedWidth) {
        head_EN.fontSize *= head_EN.fixedWidth / (head_EN.width + 1);
        head_EN.width = head_EN.fixedWidth;
    }
    // gen header TH (142 / 46)
    var head_TH = new fabric.Text(obj.front_card_nameTH, {
        fontFamily: font_name,
        fontSize: 12,
        top: 46,
        left: 142,
        textAlign: 'center',
        fixedWidth: 200,
        width: 200,

    });


    canvas.add(head_TH);
    canvas.add(head_EN);
    canvas.renderAll();
}

function gen_front_card(font_name ,obj , type ){

    if(type == "spirit01"){

    }else if (type == "spirit02"){

    }else if(type == "spirit03"){
        var level_top = 386;
        var level_left = 175;
    }else if(type == "spirit04"){

    }else {
        // error
    }

    var canvas =  getCanvas("front_card");

    // gen header Eng (72/17)
    var head_EN = new fabric.Text(obj.front_card_nameEN, {
        fontFamily: font_name,
        fontSize: 50,
        top: 10,
        left: 60,
        textAlign: 'center',
        fixedWidth: 200,
        width: 200,
    });

    if (head_EN.width > head_EN.fixedWidth) {
        head_EN.fontSize *= head_EN.fixedWidth / (head_EN.width + 1);
        head_EN.width = head_EN.fixedWidth;
    }
    // gen header TH (142 / 46)
    var head_TH = new fabric.Text(obj.front_card_nameTH, {
        fontFamily: font_name,
        fontSize: 12,
        top: 46,
        left: 142,

    });

    // gen level (X 180 / Y 404)
    var level = new fabric.Text(obj.front_card_level, {
        fontFamily: font_name,
        fontSize: 30,
        top: level_top,
        left: level_left,

    });
    // gen type (142 / 46)
    var type = new fabric.Text(obj.front_card_nameTH, {
        fontFamily: font_name,
        fontSize: 12,
        top: 46,
        left: 142,

    });
    // gen clan (142 / 46)
    var clan = new fabric.Text(obj.front_card_nameTH, {
        fontFamily: font_name,
        fontSize: 12,
        top: 46,
        left: 142,

    });


    canvas.add(head_TH);
    canvas.add(head_EN);
    canvas.add(level);
    canvas.renderAll();
}

function  gen_back_card() {
    
}


function constructor(){
    fabric.Object.prototype.toObject = (function (toObject) {
        return function () {
            return fabric.util.object.extend(toObject.call(this), {
                text: this.text,
                obj_type: this.obj_type ,
                obj_id: this.obj_id ,
                obj_order: this.obj_order,
                fontFamily: this.fontFamily,
                selectable: this.selectable,
            });
        };
    })(fabric.Object.prototype.toObject);
    fabric.Canvas.prototype.getItemByAttr = function(attr, name) {
        var object = null,
            objects = this.getObjects();
        for (var i = 0, len = this.size(); i < len; i++) {
            if (objects[i][attr] && objects[i][attr] === name) {
                object = objects[i];
                break;
            }
        }
        return object;
    };

}


function addText(font_name,containerLayers){
    var canvas =  getCanvas(current_canvas);
    $("li").removeClass("actived");
    canvas.deactivateAll();
    var new_text = new fabric.IText('Tap and Type!', {
        fontFamily: font_name,
        top: 100,
        left: 100,
    });
    new_text.on('changed', function(e) {
        new_text.text =new_text.getText();
        console.log('changed', new_text);
    });
    var id  = getUniqueID();
    new_text.text = "Tap and Type! "+id;
    new_text.obj_type ="text";
    new_text.obj_order = id;
    new_text.obj_id = containerLayers + "_"+id;

    //objectArray.push(new_text);
    addObjectToArray(current_canvas,new_text);

    canvas.add(new_text);
    canvas.setActiveObject(new_text);
    canvas.renderAll();
    console.log("Test "+new_text + " " + new_text.toObject());
    // layer
    addLayers(canvas, new_text, "text",containerLayers, containerLayers+"_"+id);
   // console.log(containerLayers + " " + JSON.stringify(canvas));
}

function addImageByURI(image_URI,containerLayers) {
    var reader = new FileReader();
    var canvas = getCanvas(current_canvas);
    $("li").removeClass("actived");
    canvas.deactivateAll();

    reader.onload = function (f) {
        var data = f.target.result;
        fabric.Image.fromURL(data, function (img) {

            var oImg = img.set({
                left: 0,
                top: 0,
                angle: 00,
                width: canvas.getWidth(),
                height: canvas.getHeight()
            });
          /*  oImg.toObject = (function (toObject) {
                return function () {
                    return fabric.util.object.extend(toObject.call(this), {
                        name: this.name,
                        obj_type: "image" ,
                        id: "",
                        order: "",
                    });
                };
            })(oImg.toObject);*/

            var id = getUniqueID() ;
            oImg.text = image_URI.name;
            oImg.obj_type= "image";
            oImg.obj_order = id ;
            oImg.obj_id  = containerLayers+"_"+id ;
           // objectArray.push(oImg);
            canvas.add(oImg);


          //  oImg.sendToBack();
            addObjectToArray(current_canvas,oImg);

            // manage imgae

            curent_frontCard_image = canvas.setActiveObject(oImg);
            canvas.renderAll();

            addLayers(canvas ,oImg, "image",containerLayers, containerLayers+"_"+id);
          //  console.log(containerLayers+" "+ image_URI.name);
        });
    };
    reader.readAsDataURL(image_URI);
}

function clear_current_canvase(){
    var canvas = getCanvas(current_canvas);
     canvas.clear()
}
function addFrame(frame_url, frame_name ,containerLayers){
    var canvas = getCanvas(current_canvas);
   // canvas.clear();

    $("li").removeClass("actived");
    canvas.deactivateAll();
    fabric.Image.fromURL(frame_url, function (img) {
        var frame = img.set({left: 0, top: 0,
            angle: 00,
            width: canvas.getWidth(),
            height: canvas.getHeight(),
            selectable: false

        });
        var id = getUniqueID() ;
        frame.obj_type ="frame";
        frame.text = frame_name;
        frame.obj_order = id;
        frame.obj_id  = containerLayers+"_"+id;
        addObjectToArray(current_canvas,frame);
       // objectArray.push(frame);
        canvas.add(frame);
        console.log("frame "+frame.toObject)
        //   manage Frame ;
        curent_frontCard_frame = canvas.setActiveObject(frame);
        canvas.renderAll();

        addLayers(canvas ,frame, "frame" ,containerLayers, containerLayers+"_"+id);


        // console.log(containerLayers+" "+JSON.stringify(canvas))
    });
}

function manageFrame_Image() {
    var canvas =  getCanvas(current_canvas);
    if(current_canvas == "front_card"){
        alert(curent_frontCard_frame +" "+curent_frontCard_image)
        if(curent_frontCard_frame != null && curent_frontCard_image  != null){
            alert("curent_frontCard_frame group curent_frontCard_image")
            var group = new fabric.Group([curent_frontCard_frame, curent_frontCard_image], {
            });
            canvas.clear().renderAll();
            canvas.add(group).renderAll();
        }
    }else if(current_canvas == "back_card"){
        if(curent_backCard_frame!= null && curent_backCard_image  != null){
            curent_backCard_image.globalCompositeOperation = 'source-atop';
        }
    }
    canvas.renderAll.bind(canvas)
}

function getCanvas(canvas_name) {
    if (canvas_name == "front_card") {
        return frontCard_canvas;
    } else {
        return backCard_canvas;
    }
}

function addLayers(canvas ,elementType, name,containerLayers , id) {
    var li_id = id;
    $("#"+containerLayers).prepend('<li id="'+li_id +'" class="layers ui-state-default actived"> '+ id+"\n"+name +'</li>');
    // remove active state of each layer
    $("li").removeClass("actived");
    canvas.deactivateAll();



    $("#"+li_id).click(function (evt) {
        if(containerLayers=="frontLayers"){
            focusFrontCard()
        }else{
            focusBackCard()
        }
        if ($(this).hasClass("actived")) {
            // remove active state of all layers and objects
            $("li").removeClass("actived");
            canvas.deactivateAll();
            canvas.renderAll();
        }
        else {
            // remove active state of all layers and objects
            $("li").removeClass("actived");
            canvas.deactivateAll();
            canvas.renderAll();
            // activate layer and object
            $(this).addClass("actived");

          var obj =canvas.getItemByAttr('obj_id', li_id)
            console.log("Select "+obj.obj_id+" "+obj.text)
            canvas.setActiveObject(obj);
           canvas.renderAll();
        }
    });

    elementType.on('selected', function() {
        $("li").removeClass("actived");
        $("#"+li_id).addClass("actived");
    });
}



function reload_page(clear_all) {


    /*if(clear_all == "false"){

        $("#frontLayers").empty();
        $("#backLayers").empty();
        FrontobjectArray = [];
        BackobjectArray = [];
        frontCard_canvas.clear();
        backCard_canvas.clear();

        $("#Card_name").val("");
        $("#Card_comment").val("");
    }else{

        $("#frontLayers").empty();
        $("#backLayers").empty();
        FrontobjectArray = [];
        BackobjectArray = [];
        frontCard_canvas.clear();
        backCard_canvas.clear();
        cardListTable.ajax.reload();
        $("#Card_name").val("");
        $("#Card_comment").val("");
    }*/

    $("#frontLayers").empty();
    $("#backLayers").empty();
    FrontobjectArray = [];
    BackobjectArray = [];
    frontCard_canvas.clear();
    backCard_canvas.clear();
    cardListTable.ajax.reload();
    $("#Card_name").val("");
    $("#Card_comment").val("");

}
function addObjectToArray(  canvas, object ) {
        if(canvas == "front_card"){
           FrontobjectArray.push(object);
        }else{
           BackobjectArray.push(object);
        }
}

function save_work() {

}

// select all objects
function deleteObjects(){
    var canvas = getCanvas(current_canvas);
    var activeObject = canvas.getActiveObject();

    if (activeObject) {
        $('#'+activeObject.obj_id).remove()
        canvas.remove(activeObject);
        notification_ok("Delete :"+activeObject.obj_id +" "+activeObject.text);
    }else{
        notification_error("Nothing selected");
    }

}

function getUniqueID(){
    var today = formatDate(new Date());
    return today;
}
function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function formatDate(date) {
    /*var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = addZero(date.getSeconds());
    var strTime = hours + '' + minutes+''+seconds;
    return  date.getFullYear()+date.getMonth()+1 + "" + date.getDate() + "" + +strTime ;*/

    var utc_timestamp = Math.floor(Date.now() / 1000);
    return utc_timestamp;
}

function focusFrontCard() {
    containerLayers = "frontLayers";
    current_canvas = "front_card";
   // $('#div_back_card').removeClass("highlight");
   // $('#div_front_card').addClass("highlight");
    backCard_canvas.deactivateAll().renderAll();
}
function focusBackCard() {
    containerLayers = "backLayers";
    current_canvas = "back_card";
    frontCard_canvas.deactivateAll().renderAll();
    //$('#div_front_card').removeClass("highlight");
   // $('#div_back_card').addClass("highlight");
}

function getCardType(cardName){
    var pieces = cardName.split(".");
    return pieces[pieces.length-1];
}